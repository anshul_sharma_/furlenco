import requests
from random import randint, random
import time

def strTimeProp(start, end, format, prop):
    stime = time.mktime(time.strptime(start, format))
    etime = time.mktime(time.strptime(end, format))
    ptime = stime + prop * (etime - stime)
    return time.strftime("%Y-%m-%dT%H:%M:%S", time.localtime(ptime))


def randomDate(start, end, prop):
    return strTimeProp(start, end, '%m/%d/%Y %I:%M %p', prop)

base_url = "http://127.0.0.1:9200/furlenco_v5/"
useragents = ["Mozilla/5.0 (X11; Linux x86_64; rv:6.0a1) Gecko/20110421 Firefox/6.0a1", "IE8.0", "Safari", "Chrome", "NULL"]
ips = ["192.1.164.1", "192.1.164.2", "192.1.164.3", "192.1.164.4", "192.1.164.5", "192.1.162.5", "192.1.162.100", "192.1.162.205"]
sessionIds = ["ACQPH776666", "ACQPH775555", "ACQPH774444", "ACQPH773333", "681967"]
browserIds = ["1234", "5678", "9012", "3456", "7826", "52637", "231876", "6218"]
site_urls = ["http://furlenco.com/categories", "http://furlenco.com/products","http://furlenco.com/packages", "http://furlenco.com/build"]
locations = ["{\"coordinates\":{\"lat\":36.518375,\"lon\":-86.05828083}}", "{\"coordinates\":{\"lat\":12.9833,\"lon\":77.5833}}", "{\"coordinates\":{\"lat\":28.6139,\"lon\":77.2090}}", "{\"coordinates\":{\"lat\":51.5072,\"lon\":-0.1275}}", "{\"coordinates\":{\"lat\":-33.8650,\"lon\":151.2094}}", "{\"coordinates\":{\"lat\":35.6833,\"lon\":139.6833}}"]

for i in xrange(0, 1000):
	url = base_url + str(i)
	index = randint(0, len(useragents)-1)
	useragent = useragents[index]
	index = randint(0, len(ips)-1)
	ip = ips[index]
	index = randint(0, len(sessionIds)-1)
	sessionId = sessionIds[index]
	index = randint(0, len(browserIds)-1)
	browserId = browserIds[index]
	index = randint(0, len(site_urls)-1)
	site_url = site_urls[index]
	index = randint(0, len(locations)-1)
	location = locations[index]
	timestamp = randomDate("03/12/2016 12:01 AM", "03/12/2016 11:59 PM", random()) + ".000Z"
	payload = "{\"useragent\":\"" + useragent + "\",\"IP\":\""+ ip +"\",\"sessionId\":\"" + sessionId +"\",\"URL\":\"" + site_url + "\",\"browserId\":\""+ browserId +"\",\"geo\":"+ location + ",\"utc_time\":\"" + str(timestamp) + "\"}"
	headers = {
	    'content-type': "application/json",
	    'cache-control': "no-cache",
	    'postman-token': "ad403018-69fa-f2a7-0ae9-a8e0d34073f5"
	    }
	#print payload
	response = requests.request("POST", url, data=payload, headers=headers)
	print(response.text)
